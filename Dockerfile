FROM node:20-alpine
RUN apk --update upgrade && \
    apk add \
    bash \
    git \
    make \
    g++ \
    python3 \
    openssh

WORKDIR /app

COPY . .

RUN npm install -g yarn --force

RUN yarn

COPY . .

EXPOSE 3000

CMD ["yarn", "run:prod"]
