"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Camera = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const entry_point_model_1 = __importDefault(require("./entry_point.model"));
let Camera = class Camera extends sequelize_typescript_1.Model {
    constructor() {
        super(...arguments);
        this.id = 0;
        this.endpoint = "";
        this.type = 0;
        this.entryPointId = 0;
    }
};
exports.Camera = Camera;
__decorate([
    (0, sequelize_typescript_1.Column)({
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_typescript_1.DataType.INTEGER,
    })
], Camera.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false,
    })
], Camera.prototype, "endpoint", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: false,
    })
], Camera.prototype, "type", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: false,
    })
], Camera.prototype, "entryPointId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => entry_point_model_1.default, "id")
], Camera.prototype, "entryPoint", void 0);
exports.Camera = Camera = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: 'cameras',
        timestamps: true,
        paranoid: true,
    })
], Camera);
exports.default = Camera;
