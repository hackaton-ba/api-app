"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntryPoint = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const camera_model_1 = require("./camera.model");
const entry_model_1 = __importDefault(require("./entry.model"));
let EntryPoint = class EntryPoint extends sequelize_typescript_1.Model {
    constructor() {
        super(...arguments);
        this.id = 0;
        this.name = "";
        this.lat = 0;
        this.lng = 0;
    }
};
exports.EntryPoint = EntryPoint;
__decorate([
    (0, sequelize_typescript_1.Column)({
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_typescript_1.DataType.INTEGER,
    })
], EntryPoint.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false,
    })
], EntryPoint.prototype, "name", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.REAL,
        allowNull: false,
    })
], EntryPoint.prototype, "lat", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.REAL,
        allowNull: false,
    })
], EntryPoint.prototype, "lng", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => camera_model_1.Camera, "cameraId")
], EntryPoint.prototype, "cameras", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => entry_model_1.default, "entryPointId")
], EntryPoint.prototype, "entries", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => entry_model_1.default, "exitPointId")
], EntryPoint.prototype, "exits", void 0);
exports.EntryPoint = EntryPoint = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: 'entry_points',
        timestamps: true,
        paranoid: true,
    })
], EntryPoint);
exports.default = EntryPoint;
