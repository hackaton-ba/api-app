"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Notification = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const user_model_1 = __importDefault(require("./user.model"));
const entry_model_1 = __importDefault(require("./entry.model"));
const vehicle_model_1 = __importDefault(require("./vehicle.model"));
const penalty_model_1 = __importDefault(require("./penalty.model"));
const document_model_1 = __importDefault(require("./document.model"));
const request_model_1 = __importDefault(require("./request.model"));
let Notification = class Notification extends sequelize_typescript_1.Model {
    constructor() {
        super(...arguments);
        this.id = 0;
    }
};
exports.Notification = Notification;
__decorate([
    (0, sequelize_typescript_1.Column)({
        primaryKey: true,
        type: sequelize_typescript_1.DataType.INTEGER,
        autoIncrement: true
    })
], Notification.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true
    })
], Notification.prototype, "title", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true,
    })
], Notification.prototype, "body", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "type", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "userId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "entryId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "requestId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "vehicleId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "penaltyId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Notification.prototype, "documentId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => user_model_1.default, "id")
], Notification.prototype, "user", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => entry_model_1.default, "id")
], Notification.prototype, "entry", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => vehicle_model_1.default, "id")
], Notification.prototype, "vehicle", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => penalty_model_1.default, "id")
], Notification.prototype, "penalty", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => document_model_1.default, "id")
], Notification.prototype, "document", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => request_model_1.default, "id")
], Notification.prototype, "request", void 0);
exports.Notification = Notification = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: 'notifications',
        timestamps: true,
        paranoid: true,
    })
], Notification);
exports.default = Notification;
