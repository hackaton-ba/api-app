"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Request = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const vehicle_model_1 = __importDefault(require("./vehicle.model"));
const payment_model_1 = require("./payment.model");
const entry_model_1 = require("./entry.model");
const document_model_1 = __importDefault(require("./document.model"));
let Request = class Request extends sequelize_typescript_1.Model {
    constructor() {
        super(...arguments);
        this.id = 0;
    }
};
exports.Request = Request;
__decorate([
    (0, sequelize_typescript_1.Column)({
        primaryKey: true,
        type: sequelize_typescript_1.DataType.INTEGER,
        autoIncrement: true
    })
], Request.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Request.prototype, "type", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true,
    })
], Request.prototype, "parkingSpot", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true,
    })
], Request.prototype, "stoppingSpot", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true,
    })
], Request.prototype, "status", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true,
    })
], Request.prototype, "vehicleId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => vehicle_model_1.default, "id")
], Request.prototype, "vehicle", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => payment_model_1.Payment, "requestId")
], Request.prototype, "payments", void 0);
__decorate([
    (0, sequelize_typescript_1.HasOne)(() => entry_model_1.Entry, "requestId")
], Request.prototype, "entry", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => document_model_1.default, "requestId")
], Request.prototype, "documents", void 0);
exports.Request = Request = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: 'requests',
        timestamps: true,
        paranoid: true,
    })
], Request);
exports.default = Request;
