"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Entry = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const entry_point_model_1 = __importDefault(require("./entry_point.model"));
const request_model_1 = __importDefault(require("./request.model"));
const media_model_1 = __importDefault(require("./media.model"));
const penalty_model_1 = __importDefault(require("./penalty.model"));
let Entry = class Entry extends sequelize_typescript_1.Model {
    constructor() {
        super(...arguments);
        this.id = 0;
        this.entryDate = new Date();
        this.exitDate = new Date();
        this.entryPointId = 0;
        this.exitPointId = 0;
        this.requestId = 0;
    }
};
exports.Entry = Entry;
__decorate([
    (0, sequelize_typescript_1.Column)({
        primaryKey: true,
        type: sequelize_typescript_1.DataType.INTEGER,
        autoIncrement: true
    })
], Entry.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DATE,
        allowNull: true
    })
], Entry.prototype, "entryDate", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DATE,
        allowNull: true
    })
], Entry.prototype, "exitDate", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true
    })
], Entry.prototype, "entryPointId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => entry_point_model_1.default, "id")
], Entry.prototype, "entryPoint", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true
    })
], Entry.prototype, "exitPointId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => entry_point_model_1.default, "id")
], Entry.prototype, "exitPoint", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: true
    })
], Entry.prototype, "requestId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => request_model_1.default, "id")
], Entry.prototype, "request", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => media_model_1.default, "entryId")
], Entry.prototype, "media", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => penalty_model_1.default, "entryId")
], Entry.prototype, "penalties", void 0);
exports.Entry = Entry = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: 'entries',
        timestamps: true,
        paranoid: true,
    })
], Entry);
exports.default = Entry;
