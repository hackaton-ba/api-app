"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const database_1 = __importDefault(require("./database"));
const port = process.env.PORT || 3000;
app_1.app.listen(port, () => {
    console.log(`Api listening at http://localhost:${port}`);
    database_1.default.sync().then(() => {
        console.log('Database & tables created!');
    });
});
