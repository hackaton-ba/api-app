import bcrypt from 'bcryptjs'

const BCRYPT_WORK_FACTOR_BASE = 12
const BCRYPT_DATE_BASE = 1483228800000
const BCRYPT_WORK_INCREASE_INTERVAL = 47300000000

export default async (password: string): Promise<string | undefined> => {
	try {
		const BCRYPT_CURRENT_DATE = new Date().getTime()
		// eslint-disable-next-line
		const BCRYPT_WORK_INCREASE = Math.max(0, Math.floor((BCRYPT_CURRENT_DATE - BCRYPT_DATE_BASE) / BCRYPT_WORK_INCREASE_INTERVAL))
		const BCRYPT_WORK_FACTOR = Math.min(19, BCRYPT_WORK_FACTOR_BASE + BCRYPT_WORK_INCREASE)

		const salt = await bcrypt.genSalt(BCRYPT_WORK_FACTOR)
		const hashedPassword = await bcrypt.hash(password, salt)
		return hashedPassword
	} catch (e) {
		return
	}
}