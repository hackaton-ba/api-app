import User from "../database/models/user.model";

export class MailService {
    async sendMail(params: {
        to: string;
        subject: string;
        text: string;
    }) {
        const { to, subject, text } = params;

        // TODO Out of scope: Send email

        return;
    }

    async sendVerificationEmail(params: {
        email: string;
        user: User;
    }) {
        const { email, user } = params;
        const { name, surname } = user;

        // TODO mock
        const verificationCode = "123456";

        const subject = "Verify your email";
        const text = `Hello ${name} ${surname},\n\nPlease verify your email by clicking the following link:\n\nhttp://localhost:3000/verify?code=${verificationCode}`;

        await this.sendMail({
            to: email,
            subject,
            text,
        });

        return;
    }
}

export const mailService = new MailService();