import User from "../database/models/user.model";
import { UserTypes } from "../types/user.types";
import hash from "../utils/hash";

export type FindOptions = {
    limit?: number;
    offset?: number;
} & Partial<User>;

export class UserService {
    userDto = User;

    constructor() {

    }

    async create(user: Partial<User>): Promise<User> {
        return await this.userDto.create(user);
    }

    async find(params: FindOptions): Promise<User[]> {

        const { limit, offset, ...where } = params

        return await this.userDto.findAll({
            where,
            limit,
            offset
        });
    }

    async register(params: {
        email: string;
        name: string;
        surname: string;
        password: string;
        phone?: string;
    }): Promise<User> {
        const { email, name, surname, password, phone } = params;

        const user = await this.userDto.create({
            email,
            name,
            surname,
            password: await hash(password),
            phone,
            type: UserTypes.USER,
        });
        
        return user;
    }

    async login(params: {
        email: string,
        password: string,
    }) {
        const { email, password } = params;

        const user = await this.userDto.findOne({
            where: {
                email,
            }
        });

        if (!user) {
            throw new Error("User not found");
        }

        const validPassword = await hash(password);

        if (!validPassword) {
            throw new Error("Invalid password");
        }

        if (user.password !== validPassword) {
            throw new Error("Invalid password");
        }

        return user;
    }
}

export default new UserService();
