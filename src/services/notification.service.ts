import Notification from "../database/models/notification.model";
import Request from "../database/models/request.model";
import User from "../database/models/user.model";
import Vehicle from "../database/models/vehicle.model";
import { mailService } from "./mail.service";

class NotificationService {
    public async notify(
        params: {
            title: string;
            body: string;
            userId?: number;
            requestId?: number;
            type?: number;
        }
    ) {
        const { title, body, userId, requestId, type } = params;

        const notification = await Notification.create({
            title,
            body,
            userId,
            requestId,
            type,
        });
        
        return notification;
    }

    public async accept(
        params: {
            requestId: number;
        }
    ) {
        const { requestId } = params;

        const request = await Request.findByPk(requestId, {
            include: {
                model: Vehicle,
                include: [{
                    model: User,
                }]
            }
        });

        const notification = await Notification.create({
            title: "Request accepted",
            body: "Your request has been accepted",
            requestId,
            userId: request?.vehicle?.ownerId,
        });

        mailService.sendMail({
            to: request?.vehicle?.user?.email!,
            subject: "Request accepted",
            text: "Your request has been accepted",
        })

        // TODO: Out of scope - Notify IS Samo
        
        return notification;
    }

    public async reject(
        params: {
            requestId: number;
        }
    ) {
        const { requestId } = params;

        const request = await Request.findByPk(requestId, {
            include: {
                model: Vehicle,
                include: [{
                    model: User,
                }]
            }
        });

        const notification = await Notification.create({
            title: "Request rejected",
            body: "Your request has been rejected",
            requestId,
            userId: request?.vehicle?.ownerId,
        });

        mailService.sendMail({
            to: request?.vehicle?.user?.email!,
            subject: "Request rejected",
            text: "Your request has been rejected",
        })
    }
}

export default new NotificationService();
