import Vehicle from "../database/models/vehicle.model";

export class VehicleService {
    async create(vehicle: {
        plate: string;
        brand: string;
        weight: string;
        ownerId: number;
    }) {
        return Vehicle.create(vehicle);
    }

    async find(params: {
        limit?: number;
        offset?: number;
    } & Partial<Vehicle>) {
        const { limit, offset, ...where } = params;

        return Vehicle.findAll({
            limit,
            offset,
            where
        });
    }
}

export default new VehicleService();