import Request from "../database/models/request.model";

export class RequestService {
    create(
        request: {
            parkingSpot?: string,
            stoppingSpot?: string,
            type?: number,
            vehicleId: number,
            userId: number,
        }
    ) {
        return Request.create(request);
    }

    list(params: {
        limit?: number;
        offset?: number;
    } & Partial<Request>) {
        const { limit, offset, ...where } = params;

        return Request.findAll({
            limit,
            offset,
            where
        });
    }

    get(params: {
        id: number;
    }) {
        const { id } = params;

        return Request.findByPk(id);
    }

    checkRequest(request: Request) {
        // TODO Out of scope, no registry to check against

        return false;
    }
}

export default new RequestService();