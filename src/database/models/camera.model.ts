import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import EntryPoint from "./entry_point.model";

@Table({
    tableName: 'cameras',
    timestamps: true,
    paranoid: true,
})
export class Camera extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
        type: DataType.INTEGER,
    })
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    endpoint: string = "";

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    type: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    entryPointId: number = 0;

    @BelongsTo(() => EntryPoint, "id")
    entryPoint?: EntryPoint;
}

export default Camera;