import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import Entry from "./entry.model";

@Table({
    tableName: 'media',
    timestamps: true,
    paranoid: true,
})
export class Media extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    url?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    type?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    entryId?: number;

    @BelongsTo(() => Entry, "id")
    entry?: Entry;
}

export default Media;
