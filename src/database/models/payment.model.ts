import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import Request from "./request.model";

@Table({
    tableName: 'payments',
    timestamps: true,
    paranoid: true,
})
export class Payment extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
        type: DataType.INTEGER,
    })
    id: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    amount: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    status: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    variableSymbol: string = "";

    @Column({
        type: DataType.DATE,
        allowNull: false,
    })
    dueDate: Date = new Date();

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    requestId?: number;

    @BelongsTo(() => Request, "id")
    request?: Request;
}

export default Payment;
