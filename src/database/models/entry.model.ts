import { BelongsTo, Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import EntryPoint from "./entry_point.model";
import Request from "./request.model";
import Media from "./media.model";
import Penalty from "./penalty.model";

@Table({
    tableName: 'entries',
    timestamps: true,
    paranoid: true,
})
export class Entry extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.DATE,
        allowNull: true
    })
    entryDate: Date = new Date();

    @Column({
        type: DataType.DATE,
        allowNull: true
    })
    exitDate: Date = new Date();

    @Column({
        type: DataType.INTEGER,
        allowNull: true
    })
    entryPointId: number = 0;

    @BelongsTo(() => EntryPoint, "id")
    entryPoint?: EntryPoint;

    @Column({
        type: DataType.INTEGER,
        allowNull: true
    })
    exitPointId: number = 0;

    @BelongsTo(() => EntryPoint, "id")
    exitPoint?: EntryPoint;

    @Column({
        type: DataType.INTEGER,
        allowNull: true
    })
    requestId: number = 0;

    @BelongsTo(() => Request, "id")
    request?: Request;

    @HasMany(() => Media, "entryId")
    media?: Media[];

    @HasMany(() => Penalty, "entryId")
    penalties?: Penalty[];
}

export default Entry;
