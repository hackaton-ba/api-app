import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import Request from "./request.model";

@Table({
    tableName: 'documents',
    timestamps: true,
    paranoid: true,
})
export class Document extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    type?: number;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    url?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    name?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    requestId?: number;

    @BelongsTo(() => Request, "id")
    request?: Request;
}
export default Document;
