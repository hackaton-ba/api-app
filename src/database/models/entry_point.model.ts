import { Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import { Camera } from "./camera.model";
import Entry from "./entry.model";

@Table({
    tableName: 'entry_points',
    timestamps: true,
    paranoid: true,
})
export class EntryPoint extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
        type: DataType.INTEGER,
    })
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    name: string = "";

    @Column({
        type: DataType.REAL,
        allowNull: false,
    })
    lat: number = 0;


    @Column({
        type: DataType.REAL,
        allowNull: false,
    })
    lng: number = 0;

    @HasMany(() => Camera, "cameraId")
    cameras?: Camera[];

    @HasMany(() => Entry, "entryPointId")
    entries?: Entry[];

    @HasMany(() => Entry, "exitPointId")
    exits?: Entry[];
    
}

export default EntryPoint;
