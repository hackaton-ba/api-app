import { Column, DataType, HasMany, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Vehicle } from "./vehicle.model";

@Table({
    tableName: "users",
    timestamps: true,
    paranoid: true,
})
export class User extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id?: number;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    name?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    surname?: string;
    
    @Column({
        type: DataType.STRING,
        allowNull: true,
        unique: true,
    })
    email?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    password?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        unique: true,
    })
    phone?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    address?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    identifier?: string

    @Column(
        {
            type: DataType.INTEGER,
        }
    )
    type: number = 0;

    @HasMany(() => Vehicle, "ownerId")
    vehicles?: Vehicle[];
}
 
export default User;
