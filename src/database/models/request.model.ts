import { BelongsTo, Column, DataType, HasMany, HasOne, Model, Table } from "sequelize-typescript";
import Vehicle from "./vehicle.model";
import { Payment } from "./payment.model";
import { Entry } from "./entry.model";
import Document from "./document.model";

@Table({
    tableName: 'requests',
    timestamps: true,
    paranoid: true,
})
export class Request extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    type?: number;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    parkingSpot?: string

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    stoppingSpot?: string

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    status?: string

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    vehicleId?: number

    @BelongsTo(() => Vehicle, "id")
    vehicle?: Vehicle;

    @HasMany(() => Payment, "requestId")
    payments?: Payment[];

    @HasOne(() => Entry, "requestId")
    entry?: Entry;

    @HasMany(() => Document, "requestId")
    documents?: Document[];
}

export default Request;
