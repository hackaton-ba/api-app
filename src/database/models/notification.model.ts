import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import User from "./user.model";
import Entry from "./entry.model";
import Vehicle from "./vehicle.model";
import Penalty from "./penalty.model";
import Document from "./document.model";
import Request from "./request.model";

@Table({
    tableName: 'notifications',
    timestamps: true,
    paranoid: true,
})
export class Notification extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    title?: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    body?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    type?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    userId?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    entryId?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    requestId?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    vehicleId?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    penaltyId?: number;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    documentId?: number;

    @BelongsTo(() => User, "id")
    user?: User;

    @BelongsTo(() => Entry, "id")
    entry?: Entry;

    @BelongsTo(() => Vehicle, "id")
    vehicle?: Vehicle;

    @BelongsTo(() => Penalty, "id")
    penalty?: Penalty;

    @BelongsTo(() => Document, "id")
    document?: Document;

    @BelongsTo(() => Request, "id")
    request?: Request;
}

export default Notification;
