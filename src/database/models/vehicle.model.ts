import { BelongsTo, BelongsToMany, Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import User from "./user.model";
import Request from "./request.model";

@Table({
    tableName: "vehicles",
    timestamps: true,
    paranoid: true,
})
export class Vehicle extends Model {
    @Column(
        {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        }
    )
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    brand?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    weight?: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    plate: string = "";

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    ownerId?: number;

    @BelongsTo(() => User, "id")
    user?: User;

    @HasMany(() => Request, "vehicleId")
    requests?: Request[];
}
export default Vehicle;
