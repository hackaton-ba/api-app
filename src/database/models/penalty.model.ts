import { BelongsTo, Column, DataType, Model, Table } from "sequelize-typescript";
import Entry from "./entry.model";

@Table({
    tableName: 'penalties',
    timestamps: true,
    paranoid: true,
})
export class Penalty extends Model {
    @Column({
        primaryKey: true, 
        type: DataType.INTEGER, 
        autoIncrement: true
    })
    id: number = 0;

    @Column({
        type: DataType.STRING,
        allowNull: true
    })
    reason?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    status?: number;

    @Column({
        type: DataType.STRING,
        allowNull: true,
    })
    variableSymbol?: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    amount?: number = 0;

    @Column({
        type: DataType.INTEGER,
        allowNull: true,
    })
    entryId?: number;

    @BelongsTo(() => Entry, "id")
    entry?: Entry;

}

export default Penalty;