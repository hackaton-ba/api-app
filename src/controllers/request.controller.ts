import { Route, Controller, Post, Body, Get, Query, Path } from "tsoa";
import userService from "../services/user.service";
import { UserTypes } from "../types/user.types";
import vehicleService from "../services/vehicle.service";
import requestService from "../services/request.service";
import { mailService } from "../services/mail.service";
import notificationService from "../services/notification.service";

@Route("requests")
export class RequestController extends Controller {
    @Post("")
    public async createRequest(
        @Body() requestBody: {
            //user
            name: string,
            surname: string,
            email: string,
            phone: string,
            address: string,
            identifier: string,
            userType: number,
            // request
            parkingSpot?: string;
            stoppingSpot?: string;
            type: number;
            // vehicle
            plate: string;
            brand: string;
            weight: string;
        }
    ): Promise<void> {
        const {
            name,
            surname,
            email,
            phone,
            address,
            identifier,
            parkingSpot,
            stoppingSpot,
            type,
            plate,
            brand,
            weight,
        } = requestBody;
        
        const users = await userService.find({
            email,
            limit: 1,
        })

        let [user] = users;

        if (!user) {
            user = await userService.create({
                name,
                surname,
                email,
                phone,
                identifier,
                type: UserTypes.SHADOW,
            });
        }

        const vehicles = await vehicleService.find({
            plate,
            limit: 1,
        })

        let [vehicle] = vehicles;

        if (!vehicle) {
            vehicle = await vehicleService.create({
                plate,
                brand,
                weight,
                ownerId: user.id!,
            })
        }

        const request = await requestService.create({
            parkingSpot,
            stoppingSpot,
            type,
            vehicleId: vehicle.id!,
            userId: user.id!,
        })

        const valid = requestService.checkRequest(request);

        if (!valid) {
            notificationService.notify({
                title: "Neplatná požiadavka",
                body: "Požiadavka bola zamietnutá systémom a označená pre manuálne preverenie.",
                requestId: request.id!,
            });
        }
    }

    @Get("")
    public async getRequests(
        @Query() query: {
            limit?: number;
            offset?: number;
        }
    ) {
        return requestService.list(query);
    }

    @Get("{id}")
    public async getRequest(
        @Path("id") id: number
    ) {
        return requestService.get({
            id,
        });
    }
}