import {
    Body,
    Controller,
    Get,
    Post,
    Query,
    Res,
    Route,
    TsoaResponse,
} from "tsoa";
import userService from "../services/user.service";
import hash from "../utils/hash";
import { mailService } from "../services/mail.service";
import jwt from "jsonwebtoken";

export type UserRequest = {
    name: string;
    surname: string;
    email: string;
    password: string;
    phone?: string;
    address?: string;
    identifier?: string;
    type?: number;
};

@Route("users")
export class UsersController extends Controller {
    @Post("")
    public async createUser(
        @Body() requestBody: UserRequest,
        @Res() forbiddenResult: TsoaResponse<403, { reason: string }>
    ): Promise<UserRequest> {
        const { email, name, surname, password, type } = requestBody;

        if (type === 0) {
            return forbiddenResult(403, {
                reason: "Cannot create superadmin account",
            });
        }

        const hashedPassword = await hash(password);

        if (!hashedPassword) {
            this.setStatus(400);
            throw new Error("Error while hashing password");
        }

        const user = await userService.create({
            email,
            name,
            surname,
            type,
            password: hashedPassword,
        });
        this.setStatus(200);
        return user as UserRequest;
    }

    @Post("register")
    public async registerUser(
        @Body() requestBody: UserRequest,
    ): Promise<{
        id: number;
        name?: string;
        surname?: string;
        email?: string;
        phone?: string;
        address?: string;
        identifier?: string;
        type?: number;
        token?: string;
    }> {
        const { email, name, surname, password, phone } = requestBody;

        const user = await userService.register({
            email,
            name,
            surname,
            password,
            phone,
        });

        await mailService.sendVerificationEmail({
            email,
            user
        })

        return {
            id: user.id!,
            name: user.name,
            surname: user.surname,
            email: user.email,
            phone: user.phone,
            address: user.address,
            identifier: user.identifier,
            type: user.type,
        };
    }

    @Post("login")
    public async loginUser(
        @Body() requestBody: {
            email: string,
            password: string,
        },
    ): Promise<{
        id: number;
        name?: string;
        surname?: string;
        email?: string;
        phone?: string;
        address?: string;
        identifier?: string;
        type?: number;
        token?: string;
    }> {
        const { email, password } = requestBody;

        const user = await userService.login({
            email,
            password,
        });

        const userData = {
            id: user.id!,
            name: user.name,
            surname: user.surname,
            email: user.email,
            phone: user.phone,
            address: user.address,
            identifier: user.identifier,
            type: user.type,
        }

        return {
            ...userData,
            token: jwt.sign(userData, process.env.JWT_SECRET || "psst", { expiresIn: '1h' })
        };
    }

    @Get("")
    public async getUsers(
        @Query() limit?: number,
        @Query() offset?: number
    ): Promise<UserRequest[]> {
        return userService.find({
            limit,
            offset,
        }) as Promise<UserRequest[]>;
    }
}
