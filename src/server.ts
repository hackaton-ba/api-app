import { app } from "./app";
import sequelize from "./database";

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Api listening at http://localhost:${port}`);
  sequelize.sync().then(() => {
    console.log('Database & tables created!');
  })
});