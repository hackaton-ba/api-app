export enum UserTypes {
    ADMIN = 0,
    USER = 1,
    EMPLOYEE = 2,
    POLICE = 3,
    SHADOW = 4,
}